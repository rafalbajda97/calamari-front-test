import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SimpleTooltip from './SimpleTooltip';

const CONTENT_MOCK = 'content mock';
const TOOLTIP_CONTENT_MOCK = 'tooltip content';

describe('SimpleTooltip', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <SimpleTooltip render={TOOLTIP_CONTENT_MOCK}>
        {CONTENT_MOCK}
      </SimpleTooltip>
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
