import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import RoundedInitialsAvatar from './RoundedInitialsAvatar';

const INITIALS_MOCK = 'rb';

describe('RoundedInitialsAvatar', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <RoundedInitialsAvatar initials={INITIALS_MOCK} />;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
