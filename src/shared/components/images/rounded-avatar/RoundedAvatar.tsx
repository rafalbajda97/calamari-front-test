import React, { FC, memo } from 'react';

import { IImageSources } from '@shared/models/common';

import RoundedInitialsAvatar from './components/rounded-initials-avatar';
import './RoundedAvatar.scss';

interface IOwnProps {
  avatar?: IImageSources;
  initials: string;
}

const RoundedAvatar: FC<IOwnProps> = ({ avatar, initials }: IOwnProps) => {
  if (avatar) {
    return (
      <img
        className="img-avatar"
        src={avatar.src}
        srcSet={avatar.srcSet}
        alt={initials}
      />
    );
  }
  return <RoundedInitialsAvatar initials={initials} />;
};

export default memo(RoundedAvatar);
