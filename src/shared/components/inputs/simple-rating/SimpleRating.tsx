import React, { FC, useState } from 'react';

import SingleStar from './components/single-star';

import './SimpleRating.scss';

interface IOwnProps {
  ratingValue: number;
  onChange: (value: number) => void;
  maxScale?: number;
}

const SimpleRating: FC<IOwnProps> = ({
  ratingValue,
  onChange,
  maxScale = 5,
}: IOwnProps) => {
  const [hover, setHover] = useState<number | null>(null);
  const roundedRatingValue = Math.round(ratingValue);
  return (
    <div className="rating-container">
      {[...Array(maxScale)].map((_star, id) => {
        const starValue = id + 1;
        const inputId = `rating-input-${id}`;
        return (
          <label htmlFor={inputId} key={starValue}>
            <input type="radio" id={inputId} name={inputId} value={starValue} />
            <SingleStar
              isFullStar={starValue <= (hover || roundedRatingValue)}
              mouseEnterHandler={() => setHover(starValue)}
              mouseLeaveHandler={() => setHover(null)}
              clickHandler={() => onChange(starValue)}
            />
          </label>
        );
      })}
    </div>
  );
};

export default SimpleRating;
