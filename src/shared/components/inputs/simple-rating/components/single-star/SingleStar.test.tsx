import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SingleStar from './SingleStar';

const IS_FULL_STAR_MOCK = true;
const MOUSE_ACTION_HANDLER_MOCK = (): void => {};

describe('SingleStar', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <SingleStar
        isFullStar={IS_FULL_STAR_MOCK}
        mouseEnterHandler={MOUSE_ACTION_HANDLER_MOCK}
        mouseLeaveHandler={MOUSE_ACTION_HANDLER_MOCK}
        clickHandler={MOUSE_ACTION_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
