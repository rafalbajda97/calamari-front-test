import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SimpleRating from './SimpleRating';

const RATING_VALUE_MOCK = 4.28;
const CHANGE_HANDLER_MOCK = (_val: number) => {};

describe('SimpleRating', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <SimpleRating
        ratingValue={RATING_VALUE_MOCK}
        onChange={CHANGE_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
