import { debounce } from 'lodash';
import React, {
  ChangeEvent,
  FC,
  ReactNode,
  useEffect,
  useMemo,
  useRef,
} from 'react';

import './ExtendableInput.scss';

interface IOwnProps {
  placeholder: string;
  iconTemplate: ReactNode;
  changeHandler: (value: string) => void;
  defaultValue?: string;
}

const ExtendableInput: FC<IOwnProps> = ({
  placeholder,
  iconTemplate,
  changeHandler,
  defaultValue,
}: IOwnProps) => {
  const inputRef = useRef<HTMLInputElement>() as React.MutableRefObject<
    HTMLInputElement
  >;

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.value = defaultValue || '';
    }
  }, [defaultValue, inputRef]);

  const handleChange = useMemo(() => {
    const debounced = debounce((event: ChangeEvent<HTMLInputElement>): void => {
      changeHandler(event.target.value);
    }, 300);
    return (event: ChangeEvent<HTMLInputElement>) => {
      event.persist();
      return debounced(event);
    };
  }, [changeHandler]);

  return (
    <div className="extendable-input-container">
      {iconTemplate}
      <input
        className="extendable-input-container__input"
        placeholder={placeholder}
        defaultValue={defaultValue}
        ref={inputRef}
        onChange={handleChange}
      />
    </div>
  );
};

export default ExtendableInput;
