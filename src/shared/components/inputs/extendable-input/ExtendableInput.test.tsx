import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SearchIcon from '@assets/icons/search/search.png';
import SearchIcon2x from '@assets/icons/search/search@2x.png';
import SearchIcon3x from '@assets/icons/search/search@3x.png';

import ExtendableInput from './ExtendableInput';

const PLACEHOLDER_MOCK = 'placeholder';
const CHANGE_HANDLER_MOCK = (_val: string): void => {};
const ICON_TEMPLATE_MOCK = (
  <img
    src={SearchIcon}
    srcSet={`${SearchIcon2x} 2x, ${SearchIcon3x} 3x`}
    alt="icon template mock"
  />
);

describe('ExtendableInput', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <ExtendableInput
        placeholder={PLACEHOLDER_MOCK}
        iconTemplate={ICON_TEMPLATE_MOCK}
        changeHandler={CHANGE_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
