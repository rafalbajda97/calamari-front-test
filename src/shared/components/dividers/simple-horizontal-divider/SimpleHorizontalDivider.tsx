import React, { FC, memo } from 'react';

import './SimpleHorizontalDivider.scss';

const SimpleHorizontalDivider: FC = () => {
  return <hr className="simple-horizontal-divider" />;
};

export default memo(SimpleHorizontalDivider);
