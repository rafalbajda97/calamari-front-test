import React, { FC, ReactNode } from 'react';

import { addAdditionalClass } from '@shared/utils/styleOperations';

import './ToggleButton.scss';

interface IOwnProps {
  children: ReactNode;
  additionalClass?: string;
  selected?: boolean;
  clickHandler?: () => void;
}

const ToggleButton: FC<IOwnProps> = ({
  children,
  selected = false,
  additionalClass,
  clickHandler,
}: IOwnProps) => {
  const baseClass = addAdditionalClass('toggle-button', additionalClass);
  const className = selected ? `${baseClass} selected` : baseClass;
  return (
    <button className={className} type="button" onClick={clickHandler}>
      {children}
    </button>
  );
};

export default ToggleButton;
