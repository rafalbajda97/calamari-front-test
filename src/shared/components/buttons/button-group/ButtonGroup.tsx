import React, { FC, memo, ReactNode } from 'react';

import { addAdditionalClass } from '@shared/utils/styleOperations';

import './ButtonGroup.scss';

interface IOwnProps {
  children: ReactNode;
  additionalClass?: string;
}

const ButtonGroup: FC<IOwnProps> = ({
  children,
  additionalClass,
}: IOwnProps) => {
  const className = addAdditionalClass('group-container', additionalClass);
  return <div className={className}>{children}</div>;
};

export default memo(ButtonGroup);
