import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import IconButton from './IconButton';

const BUTTON_TEXT_MOCK = 'button';

describe('IconButton', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <IconButton>{BUTTON_TEXT_MOCK}</IconButton>;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
