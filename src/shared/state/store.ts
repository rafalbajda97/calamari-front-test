import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import { ISpecialistsState } from '@pages/home/state';

import rootSaga from '@shared/state/sagas';

import rootReducer from './reducer';

export interface IState {
  specialists: ISpecialistsState;
}

/*
 * If it will be necessary this code can be rewritten to function configureStore(initialStore).
 * Devtools accessibility can be adjusted in the future.
 */
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

export default store;
