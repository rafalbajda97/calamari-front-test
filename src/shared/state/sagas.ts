import { all } from 'redux-saga/effects';

import { setListStateSaga } from '@pages/home/state/sagas';

function* rootSaga(): Generator {
  yield all([setListStateSaga()]);
}

export default rootSaga;
