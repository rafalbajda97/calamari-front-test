import React, { ComponentType, ReactElement } from 'react';
import { Provider } from 'react-redux';

import store from '@shared/state/store';

export const withStoreComponent = (Component: ComponentType): ReactElement => (
  <Provider store={store}>
    <Component />
  </Provider>
);

export const withStoreElement = (Element: ReactElement): ReactElement => (
  <Provider store={store}>{Element}</Provider>
);
