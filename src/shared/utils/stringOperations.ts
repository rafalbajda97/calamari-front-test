export const displayNumberWithComma = (num: number): string =>
  num.toString().replace('.', ',');
