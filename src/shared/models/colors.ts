export interface IInitialsAvatarColor {
  backgroundColor: string;
  textColor: string;
}

export const INITIALS_AVATARS_COLORS_CONFIG: IInitialsAvatarColor[] = [
  {
    backgroundColor: '#DEEAFF',
    textColor: '#3540ff',
  },
  {
    backgroundColor: '#FFF5DE',
    textColor: '#ffaf14',
  },
];
