import { capitalize } from 'lodash';

import { SpecialistsListTexts } from '@pages/home/components/specialists-list/models';
import { ISpecialist } from '@pages/home/state';

export const getSpecialistInitials = (specialist: ISpecialist): string => {
  const { firstName, lastName } = specialist;
  return `${firstName.charAt(0)}${lastName.charAt(0)}`;
};

export const getFullName = (specialist: ISpecialist): string => {
  const { firstName, lastName } = specialist;
  return `${capitalize(firstName)} ${capitalize(lastName)}`;
};

export const getDoctorTooltipText = (isFavorite: boolean): string =>
  isFavorite
    ? SpecialistsListTexts.REMOVE_FROM_FAVORITES
    : SpecialistsListTexts.ADD_TO_FAVORITES;
