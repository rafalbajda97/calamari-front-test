import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import { SPECIALISTS_MOCK } from '@pages/home/state/mocks';

import { withStoreElement } from '@shared/utils/testHelpers';

import SpecialistsList from './SpecialistsList';

describe('SpecialistsList', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = withStoreElement(
      <SpecialistsList specialists={SPECIALISTS_MOCK} />,
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
