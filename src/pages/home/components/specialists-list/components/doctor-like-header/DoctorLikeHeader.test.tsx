import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import DoctorLikeHeader from './DoctorLikeHeader';

const IS_FAVORITE_MOCK = true;
const LIKE_CLICK_HANDLER_MOCK = () => {};

describe('DoctorLikeHeader', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <DoctorLikeHeader
        isFavorite={IS_FAVORITE_MOCK}
        likeClickHandler={LIKE_CLICK_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
