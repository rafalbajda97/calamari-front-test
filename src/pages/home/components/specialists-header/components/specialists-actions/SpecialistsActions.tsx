import React, { FC } from 'react';

import { SpecialistsListStates } from '@pages/home/state';

import SearchIcon from '@assets/icons/search/search.png';
import SearchIcon2x from '@assets/icons/search/search@2x.png';
import SearchIcon3x from '@assets/icons/search/search@3x.png';

import ButtonGroup from '@shared/components/buttons/button-group';
import ToggleButton from '@shared/components/buttons/toggle-button';
import ExtendableInput from '@shared/components/inputs/extendable-input';

import { SpecialistsHeaderTexts } from '../../models';

import './SpecialistsActions.scss';

interface IOwnProps {
  listState: SpecialistsListStates;
  searchFilter: string;
  toggleListClickHandler: (state: SpecialistsListStates) => void;
  searchFilterChangeHandler: (value: string) => void;
}

const SpecialistsActions: FC<IOwnProps> = ({
  listState,
  searchFilter,
  toggleListClickHandler,
  searchFilterChangeHandler,
}: IOwnProps) => {
  const isToggleButtonSelected = (value: SpecialistsListStates): boolean =>
    listState === value;

  const allFavoriteClickHandler = (): void => {
    toggleListClickHandler(SpecialistsListStates.ALL_FAVORITE);
  };

  const mySpecialistsClickHandler = (): void => {
    toggleListClickHandler(SpecialistsListStates.MY_SPECIALISTS);
  };

  return (
    <div className="specialists-actions-container">
      <ButtonGroup>
        <ToggleButton
          additionalClass="specialists-actions-container__toggle-button"
          selected={isToggleButtonSelected(SpecialistsListStates.ALL_FAVORITE)}
          clickHandler={allFavoriteClickHandler}
        >
          {SpecialistsHeaderTexts.ALL_FAVORITE}
        </ToggleButton>
        <ToggleButton
          additionalClass="specialists-actions-container__toggle-button"
          selected={isToggleButtonSelected(
            SpecialistsListStates.MY_SPECIALISTS,
          )}
          clickHandler={mySpecialistsClickHandler}
        >
          {SpecialistsHeaderTexts.MY_SPECIALISTS}
        </ToggleButton>
      </ButtonGroup>
      <ExtendableInput
        placeholder={SpecialistsHeaderTexts.SEARCH_ICON_PLACEHOLDER}
        iconTemplate={
          <img
            className="specialists-actions-container__search-icon"
            src={SearchIcon}
            srcSet={`${SearchIcon2x} 2x, ${SearchIcon3x} 3x`}
            alt={SpecialistsHeaderTexts.SEARCH_ICON_ALT}
          />
        }
        defaultValue={searchFilter}
        changeHandler={searchFilterChangeHandler}
      />
    </div>
  );
};

export default SpecialistsActions;
