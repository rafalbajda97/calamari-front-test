import { startsWith, some } from 'lodash';
import { createSelector } from 'reselect';

import { IState } from '@shared/state/store';

import { ISpecialist, SpecialistsListStates } from './models';

const specialistsListSelector = ({ specialists }: IState): ISpecialist[] =>
  specialists.list;

export const specialistsListStateSelector = ({
  specialists,
}: IState): SpecialistsListStates => specialists.listState;

export const searchFilterSelector = ({ specialists }: IState): string =>
  specialists.search;

export const specialistsSelector = createSelector(
  specialistsListSelector,
  specialistsListStateSelector,
  searchFilterSelector,
  (
    list: ISpecialist[],
    state: SpecialistsListStates,
    searchFilter: string,
  ): ISpecialist[] => {
    const source =
      state === SpecialistsListStates.MY_SPECIALISTS
        ? list.filter((specialist) => specialist.isFavorite)
        : list;

    return source.filter(({ firstName, lastName, profession }) =>
      some(
        [firstName, lastName, profession].map((val) =>
          startsWith(val.toUpperCase(), searchFilter.toUpperCase()),
        ),
      ),
    );
  },
);
