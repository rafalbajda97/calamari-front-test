import { IImageSources, IRating } from '@shared/models/common';

export const SET_SEARCH_FILTER = '[HOME] SET_SEARCH_FILTER';
export const SET_LIST_STATE = '[HOME] SET_LIST_STATE';
export const RATE_SPECIALIST = '[HOME] RATE_SPECIALIST';
export const TOGGLE_IS_FAVORITE = '[HOME] TOGGLE_IS_FAVORITE';

export interface IToggleIsFavorite {
  type: typeof TOGGLE_IS_FAVORITE;
  payload: string;
}

export interface IRateSpecialistPayload {
  ratingValue: number;
  specialistId: string;
}

export interface IRateSpecialist {
  type: typeof RATE_SPECIALIST;
  payload: IRateSpecialistPayload;
}

export interface ISetListState {
  type: typeof SET_LIST_STATE;
  payload: SpecialistsListStates;
}

export interface ISetSearchFilter {
  type: typeof SET_SEARCH_FILTER;
  payload: string;
}

export interface ISpecialist {
  id: string;
  firstName: string;
  lastName: string;
  profession: string;
  avatar?: IImageSources;
  rating: IRating;
  isFavorite: boolean;
}

export enum SpecialistsListStates {
  ALL_FAVORITE,
  MY_SPECIALISTS,
}

export type HomeActionTypes =
  | ISetSearchFilter
  | ISetListState
  | IRateSpecialist
  | IToggleIsFavorite;
