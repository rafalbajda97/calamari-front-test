import { IRating } from '@shared/models/common';

import { ISpecialist } from './models';

const calculateNewRatingValue = (
  previousRating: IRating,
  newRatingValue: number,
): number => {
  const { value, votesAmount } = previousRating;
  return (value * votesAmount + newRatingValue) / (votesAmount + 1);
};

export const updateSpecialistRating = (
  specialist: ISpecialist,
  newRatingValue: number,
): ISpecialist => {
  const previousRating = specialist.rating;
  const newValue = calculateNewRatingValue(previousRating, newRatingValue);
  const newRating = {
    value: newValue,
    votesAmount: previousRating.votesAmount + 1,
  };
  return {
    ...specialist,
    rating: newRating,
  };
};

export const updateSpecialistIsFavorite = (
  specialist: ISpecialist,
): ISpecialist => ({
  ...specialist,
  isFavorite: !specialist.isFavorite,
});
