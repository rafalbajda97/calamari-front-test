import { ISpecialist } from '@pages/home/state/models';

import EricaMorrisonAvatar from '@assets/images/avatars/ericaMorrison/avatar.png';
import EricaMorrisonAvatar2x from '@assets/images/avatars/ericaMorrison/avatar@2x.png';
import EricaMorrisonAvatar3x from '@assets/images/avatars/ericaMorrison/avatar@3x.png';

export const SPECIALISTS_MOCK: ISpecialist[] = [
  {
    id: '0',
    firstName: 'Erica',
    lastName: 'Morrison',
    profession: 'optician',
    avatar: {
      src: EricaMorrisonAvatar,
      srcSet: `${EricaMorrisonAvatar2x}, ${EricaMorrisonAvatar3x}`,
    },
    rating: {
      value: 4.2,
      votesAmount: 114,
    },
    isFavorite: false,
  },
  {
    id: '1',
    firstName: 'David',
    lastName: 'Bowie',
    profession: 'dentist',
    rating: {
      value: 4.9,
      votesAmount: 36,
    },
    isFavorite: false,
  },
  {
    id: '2',
    firstName: 'Andrew',
    lastName: 'Carter',
    profession: 'surgeon',
    /** I am using same avatar because I couldn't get an asset from zeppelin
     * for surgeon */
    avatar: {
      src: EricaMorrisonAvatar,
      srcSet: `${EricaMorrisonAvatar2x}, ${EricaMorrisonAvatar3x}`,
    },
    rating: {
      value: 4.9,
      votesAmount: 36,
    },
    isFavorite: false,
  },
  {
    id: '3',
    firstName: 'Jessica',
    lastName: 'Sullivan',
    profession: 'oncologist',
    rating: {
      value: 4.9,
      votesAmount: 36,
    },
    isFavorite: false,
  },
];
